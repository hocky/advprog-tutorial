package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public abstract class FamiliarSpell implements Spell {
    protected Familiar familiar;

    public FamiliarSpell(Familiar familiar) {
        this.familiar = familiar;
    }

    @Override
    public void undo() {
        switch(familiar.getPrevState()){
            case SEALED:
                familiar.seal();
                break;
            case ACTIVE:
                familiar.summon();
        }
    }
}
