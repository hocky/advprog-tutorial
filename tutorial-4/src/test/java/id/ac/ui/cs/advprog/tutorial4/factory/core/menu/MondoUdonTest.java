package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class MondoUdonTest {
    private Class<?> mondoUdonClass;
    @Spy
    private MondoUdon mondoUdon = new MondoUdon("MondoUdon Lezat");

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIsAMenu() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonClass.getSuperclass());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void testMondoUdonReturnsRightAttributes() {
        String flavor = mondoUdon.getFlavor().getClass().getTypeName();
        String meat = mondoUdon.getMeat().getClass().getTypeName();
        String topping = mondoUdon.getTopping().getClass().getTypeName();
        String noodle = mondoUdon.getNoodle().getClass().getTypeName();
        String name = mondoUdon.getName();
        assertEquals(flavor, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Salty");
        assertEquals(meat, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Chicken");
        assertEquals(topping, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Cheese");
        assertEquals(noodle, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Udon");
        assertEquals(name, "MondoUdon Lezat");
        verify(mondoUdon, times(1)).getName();
        verify(mondoUdon, times(1)).getMeat();
        verify(mondoUdon, times(1)).getNoodle();
        verify(mondoUdon, times(1)).getFlavor();
        verify(mondoUdon, times(1)).getTopping();
    }
}
