package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FlavorTest {
    private Class<?> FlavorClass;

    /**
     * setUp the class Object
     * @throws ClassNotFoundException self-explanatory
     */
    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        FlavorClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor");
    }

    @Test
    public void testFlavorIsAPublicInterface() {
        int classModifiers = FlavorClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testFlavorHasMakeFlavorAbstractMethod() throws Exception {
        Method getDescription = FlavorClass.getDeclaredMethod("getDescription");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }
}
