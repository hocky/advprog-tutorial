package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaFactoryTest {
    private Class<?> liyuanSobaFactoryClass;
    private LiyuanSobaFactory liyuanSobaFactory;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory");
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    public void testLiyuanSobaFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideMakeFlavorMethod() throws Exception {
        Method makeFlavor = liyuanSobaFactoryClass.getDeclaredMethod("makeFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor",
                makeFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                makeFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(makeFlavor.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryReturnsRightFlavor() {
        Flavor flavor = liyuanSobaFactory.makeFlavor();
        assertEquals(flavor.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Sweet");
        assertEquals(flavor.getDescription(), "Adding a dash of Sweet Soy Sauce...");
    }

    @Test
    public void testLiyuanSobaFactoryReturnsRightMeat() {
        Meat meat = liyuanSobaFactory.makeMeat();
        assertEquals(meat.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Beef");
        assertEquals(meat.getDescription(), "Adding Maro Beef Meat...");
    }

    @Test
    public void testLiyuanSobaFactoryReturnsRightNoodle() {
        Noodle noodle = liyuanSobaFactory.makeNoodle();
        assertEquals(noodle.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Soba");
        assertEquals(noodle.getDescription(), "Adding Liyuan Soba Noodles...");
    }

    @Test
    public void testLiyuanSobaFactoryReturnsRightTopping() {
        Topping topping = liyuanSobaFactory.makeTopping();
        assertEquals(topping.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Mushroom");
        assertEquals(topping.getDescription(), "Adding Shiitake Mushroom Topping...");
    }

}
