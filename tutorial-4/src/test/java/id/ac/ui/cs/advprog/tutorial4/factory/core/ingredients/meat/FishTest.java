package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Fish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class FishTest {
    private Class<?> fishClass;
    private Fish fish;

    @BeforeEach
    public void setUp() throws Exception {
        fishClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Fish");
        fish = new Fish();
    }

    @Test
    public void testFishIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(fishClass.getModifiers()));
    }

    @Test
    public void testFishIsAMeat() {
        Collection<Type> interfaces = Arrays.asList(fishClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat")));
    }

    @Test
    public void testFishOverrideMeatMethod() throws Exception {
        Method getDescription = fishClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testFishReturnsRightDescription() {
        String fishResult = fish.getDescription();
        assertEquals(fishResult.getClass().getName(),
                "java.lang.String");
        assertEquals(fishResult, "Adding Zhangyun Salmon Fish Meat...");
    }

}
