package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuTest {
    private Class<?> MenuClass;

    /**
     * setUp the class Object
     * @throws ClassNotFoundException self-explanatory
     */
    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        MenuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsAPublicAbstractClass() {
        int classModifiers = MenuClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }
}
