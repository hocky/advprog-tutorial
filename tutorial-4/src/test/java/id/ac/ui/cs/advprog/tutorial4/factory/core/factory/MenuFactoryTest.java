package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MenuFactoryTest {
    private Class<?> menuFactoryClass;
    private MenuFactory menuFactory;

    @BeforeEach
    public void setUp() throws Exception {
        menuFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactoryImpl");
        menuFactory = new MenuFactoryImpl();
    }

    @Test
    public void testMenuFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(menuFactoryClass.getModifiers()));
    }

    @Test
    public void testMenuFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(menuFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")));
    }

    @Test
    public void testMenuFactoryOverrideMakeMenuMethod() throws Exception {
        Class<?>[] makeMenuArgs = new Class[2];
        makeMenuArgs[0] = String.class;
        makeMenuArgs[1] = String.class;
        Method makeMenu = menuFactoryClass.getDeclaredMethod("makeMenu", makeMenuArgs);
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                makeMenu.getGenericReturnType().getTypeName());
        assertEquals(2,
                makeMenu.getParameterCount());
        assertTrue(Modifier.isPublic(makeMenu.getModifiers()));
    }

    @Test
    public void testMenuFactoryReturnsLiyuanSoba() throws ClassNotFoundException {
        Menu menu = menuFactory.makeMenu("makanan satu", "LiyuanSoba");
        assertEquals(menu.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        assertEquals(menu.getName(), "makanan satu");
    }

    @Test
    public void testMenuFactoryReturnsInuzumaRamen() throws ClassNotFoundException {
        Menu menu = menuFactory.makeMenu("makanan dua", "InuzumaRamen");
        assertEquals(menu.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        assertEquals(menu.getName(), "makanan dua");
    }

    @Test
    public void testMenuFactoryReturnsMondoUdon() throws ClassNotFoundException {
        Menu menu = menuFactory.makeMenu("makanan tiga", "MondoUdon");
        assertEquals(menu.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        assertEquals(menu.getName(), "makanan tiga");
    }

    @Test
    public void testMenuFactoryReturnsSnevnezhaShirataki() throws ClassNotFoundException {
        Menu menu = menuFactory.makeMenu("makanan empat", "SnevnezhaShirataki");
        assertEquals(menu.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
        assertEquals(menu.getName(), "makanan empat");
    }

    @Test
    public void testMenuFactoryThrowsExceptionWhenMenuNotFound() {
        Exception exception = assertThrows(ClassNotFoundException.class,
                () -> menuFactory.makeMenu("makanan lima", "BuburPengkolan"));
    }

}
