package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenFactoryTest {
    private Class<?> inuzumaRamenFactoryClass;
    private InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory");
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    @Test
    public void testInuzumaRamenFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideMakeFlavorMethod() throws Exception {
        Method makeFlavor = inuzumaRamenFactoryClass.getDeclaredMethod("makeFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor",
                makeFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                makeFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(makeFlavor.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryReturnsRightFlavor() {
        Flavor flavor = inuzumaRamenFactory.makeFlavor();
        assertEquals(flavor.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Spicy");
        assertEquals(flavor.getDescription(), "Adding Liyuan Chili Powder...");
    }

    @Test
    public void testInuzumaRamenFactoryReturnsRightMeat() {
        Meat meat = inuzumaRamenFactory.makeMeat();
        assertEquals(meat.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Pork");
        assertEquals(meat.getDescription(), "Adding Tian Xu Pork Meat...");
    }

    @Test
    public void testInuzumaRamenFactoryReturnsRightNoodle() {
        Noodle noodle = inuzumaRamenFactory.makeNoodle();
        assertEquals(noodle.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Ramen");
        assertEquals(noodle.getDescription(), "Adding Inuzuma Ramen Noodles...");
    }

    @Test
    public void testInuzumaRamenFactoryReturnsRightTopping() {
        Topping topping = inuzumaRamenFactory.makeTopping();
        assertEquals(topping.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.BoiledEgg");
        assertEquals(topping.getDescription(), "Adding Guahuan Boiled Egg Topping");
    }

}
