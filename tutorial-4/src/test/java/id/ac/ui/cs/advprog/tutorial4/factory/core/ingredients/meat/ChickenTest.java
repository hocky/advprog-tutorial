package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class ChickenTest {
    private Class<?> chickenClass;
    private Chicken chicken;

    @BeforeEach
    public void setUp() throws Exception {
        chickenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Chicken");
        chicken = new Chicken();
    }

    @Test
    public void testChickenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(chickenClass.getModifiers()));
    }

    @Test
    public void testChickenIsAMeat() {
        Collection<Type> interfaces = Arrays.asList(chickenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat")));
    }

    @Test
    public void testChickenOverrideMeatMethod() throws Exception {
        Method getDescription = chickenClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testChickenReturnsRightDescription() {
        String chickenResult = chicken.getDescription();
        assertEquals(chickenResult.getClass().getName(),
                "java.lang.String");
        assertEquals(chickenResult, "Adding Wintervale Chicken Meat...");
    }

}
