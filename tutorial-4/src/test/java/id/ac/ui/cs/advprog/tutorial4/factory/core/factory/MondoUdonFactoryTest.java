package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonFactoryTest {
    private Class<?> mondoUdonFactoryClass;
    private MondoUdonFactory mondoUdonFactory;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory");
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testMondoUdonFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testMondoUdonFactoryOverrideMakeFlavorMethod() throws Exception {
        Method makeFlavor = mondoUdonFactoryClass.getDeclaredMethod("makeFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor",
                makeFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                makeFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(makeFlavor.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryReturnsRightFlavor() {
        Flavor flavor = mondoUdonFactory.makeFlavor();
        assertEquals(flavor.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Salty");
        assertEquals(flavor.getDescription(), "Adding a pinch of salt...");
    }

    @Test
    public void testMondoUdonFactoryReturnsRightMeat() {
        Meat meat = mondoUdonFactory.makeMeat();
        assertEquals(meat.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Chicken");
        assertEquals(meat.getDescription(), "Adding Wintervale Chicken Meat...");
    }

    @Test
    public void testMondoUdonFactoryReturnsRightNoodle() {
        Noodle noodle = mondoUdonFactory.makeNoodle();
        assertEquals(noodle.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Udon");
        assertEquals(noodle.getDescription(), "Adding Mondo Udon Noodles...");
    }

    @Test
    public void testMondoUdonFactoryReturnsRightTopping() {
        Topping topping = mondoUdonFactory.makeTopping();
        assertEquals(topping.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Cheese");
        assertEquals(topping.getDescription(), "Adding Shredded Cheese Topping...");
    }

}
