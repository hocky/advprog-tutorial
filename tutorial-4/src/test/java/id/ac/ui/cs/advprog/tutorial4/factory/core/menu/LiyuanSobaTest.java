package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;
    @Spy
    private LiyuanSoba liyuanSoba = new LiyuanSoba("LiyuanSoba Lezat");

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIsAMenu() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaClass.getSuperclass());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void testLiyuanSobaReturnsRightAttributes() {
        String flavor = liyuanSoba.getFlavor().getClass().getTypeName();
        String meat = liyuanSoba.getMeat().getClass().getTypeName();
        String topping = liyuanSoba.getTopping().getClass().getTypeName();
        String noodle = liyuanSoba.getNoodle().getClass().getTypeName();
        String name = liyuanSoba.getName();
        assertEquals(flavor, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Sweet");
        assertEquals(meat, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Beef");
        assertEquals(topping, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Mushroom");
        assertEquals(noodle, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Soba");
        assertEquals(name, "LiyuanSoba Lezat");
        verify(liyuanSoba, times(1)).getName();
        verify(liyuanSoba, times(1)).getMeat();
        verify(liyuanSoba, times(1)).getNoodle();
        verify(liyuanSoba, times(1)).getFlavor();
        verify(liyuanSoba, times(1)).getTopping();
    }
}
