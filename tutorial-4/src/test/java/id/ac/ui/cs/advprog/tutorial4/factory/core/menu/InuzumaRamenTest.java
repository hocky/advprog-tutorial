package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;
    @Spy
    private InuzumaRamen inuzumaRamen = new InuzumaRamen("InuzumaRamen Lezat");

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIsAMenu() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenClass.getSuperclass());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void testInuzumaRamenReturnsRightAttributes() {
        String flavor = inuzumaRamen.getFlavor().getClass().getTypeName();
        String meat = inuzumaRamen.getMeat().getClass().getTypeName();
        String topping = inuzumaRamen.getTopping().getClass().getTypeName();
        String noodle = inuzumaRamen.getNoodle().getClass().getTypeName();
        String name = inuzumaRamen.getName();
        assertEquals(flavor, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Spicy");
        assertEquals(meat, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Pork");
        assertEquals(topping, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.BoiledEgg");
        assertEquals(noodle, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Ramen");
        assertEquals(name, "InuzumaRamen Lezat");
        verify(inuzumaRamen, times(1)).getName();
        verify(inuzumaRamen, times(1)).getMeat();
        verify(inuzumaRamen, times(1)).getNoodle();
        verify(inuzumaRamen, times(1)).getFlavor();
        verify(inuzumaRamen, times(1)).getTopping();
    }
}
