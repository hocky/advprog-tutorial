package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class ShiratakiTest {
    private Class<?> shiratakiClass;
    private Shirataki shirataki;

    @BeforeEach
    public void setUp() throws Exception {
        shiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Shirataki");
        shirataki = new Shirataki();
    }

    @Test
    public void testFishIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(shiratakiClass.getModifiers()));
    }

    @Test
    public void testFishIsANoodle() {
        Collection<Type> interfaces = Arrays.asList(shiratakiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle")));
    }

    @Test
    public void testFishOverrideNoodleMethod() throws Exception {
        Method getDescription = shiratakiClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testFishReturnsRightDescription() {
        String shiratakiResult = shirataki.getDescription();
        assertEquals(shiratakiResult.getClass().getName(),
                "java.lang.String");
        assertEquals(shiratakiResult, "Adding Snevnezha Shirataki Noodles...");
    }

}
