package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SpicyTest {
    private Class<?> spicyClass;
    private Spicy spicy;

    @BeforeEach
    public void setUp() throws Exception {
        spicyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Spicy");
        spicy = new Spicy();
    }

    @Test
    public void testSpicyIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(spicyClass.getModifiers()));
    }

    @Test
    public void testSpicyIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(spicyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor")));
    }

    @Test
    public void testSpicyOverrideFlavorMethod() throws Exception {
        Method getDescription = spicyClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSpicyReturnsRightDescription() {
        String spicyResult = spicy.getDescription();
        assertEquals(spicyResult.getClass().getName(),
                "java.lang.String");
        assertEquals(spicyResult, "Adding Liyuan Chili Powder...");
    }

}
