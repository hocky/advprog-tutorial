package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    private Class<?> orderServiceClass;

    @Mock
    OrderDrink orderDrink;

    @Mock
    OrderFood orderFood;

    @InjectMocks
    OrderService orderService = new OrderServiceImpl();

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        orderServiceClass = Class.forName(OrderServiceImpl.class.getName());
    }

    @Test
    public void testOrderServiceHasOrderDrinkMethod() throws NoSuchMethodException {
        Class<?>[] drinkArgs = new Class[1];
        drinkArgs[0] = String.class;
        Method orderDrink = orderServiceClass.getDeclaredMethod("orderADrink", drinkArgs);
        assertEquals(orderDrink.getParameterCount(), 1);
        int methodModifiers = orderDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(orderDrink.getReturnType(), void.class);
    }

    @Test
    public void testOrderServiceHasOrderFoodMethod() throws NoSuchMethodException {
        Class<?>[] foodArgs = new Class[1];
        foodArgs[0] = String.class;
        Method orderFood = orderServiceClass.getDeclaredMethod("orderAFood", foodArgs);
        assertEquals(orderFood.getParameterCount(), 1);
        int methodModifiers = orderFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(orderFood.getReturnType(), void.class);
    }

    @Test
    public void testOrderServiceHasGetDrinkMethod() throws NoSuchMethodException {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");
        assertEquals(getDrink.getParameterCount(), 0);
        int methodModifiers = getDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(getDrink.getReturnType(), OrderDrink.class);
    }


    @Test
    public void testOrderServiceHasGetFoodMethod() throws NoSuchMethodException {
        Method getFood = orderServiceClass.getDeclaredMethod("getFood");
        assertEquals(getFood.getParameterCount(), 0);
        int methodModifiers = getFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(getFood.getReturnType(), OrderFood.class);
    }

    @Test
    public void testOrderServiceOrderADrink(){
        orderService.orderADrink("Minuman");
        verify(orderDrink, times(1)).setDrink("Minuman");
    }

    @Test
    public void testOrderServiceGetFood(){
        assertEquals(orderService.getFood().getClass().getName(), orderFood.getClass().getName());
    }

    @Test
    public void testOrderServiceOrderAFood(){
        orderService.orderAFood("Makanan");
        verify(orderFood, times(1)).setFood("Makanan");
    }

    @Test
    public void testOrderServiceGetDrink(){
        assertEquals(orderService.getDrink().getClass().getName(), orderDrink.getClass().getName());
    }
}
