package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;
    @Spy
    private SnevnezhaShirataki snevnezhaShirataki = new SnevnezhaShirataki("SnevnezhaShirataki Lezat");

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIsAMenu() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiClass.getSuperclass());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void testSnevnezhaShiratakiReturnsRightAttributes() {
        String flavor = snevnezhaShirataki.getFlavor().getClass().getTypeName();
        String meat = snevnezhaShirataki.getMeat().getClass().getTypeName();
        String topping = snevnezhaShirataki.getTopping().getClass().getTypeName();
        String noodle = snevnezhaShirataki.getNoodle().getClass().getTypeName();
        String name = snevnezhaShirataki.getName();
        assertEquals(flavor, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Umami");
        assertEquals(meat, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Fish");
        assertEquals(topping, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Flower");
        assertEquals(noodle, "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Shirataki");
        assertEquals(name, "SnevnezhaShirataki Lezat");
        verify(snevnezhaShirataki, times(1)).getName();
        verify(snevnezhaShirataki, times(1)).getMeat();
        verify(snevnezhaShirataki, times(1)).getNoodle();
        verify(snevnezhaShirataki, times(1)).getFlavor();
        verify(snevnezhaShirataki, times(1)).getTopping();
    }
}
