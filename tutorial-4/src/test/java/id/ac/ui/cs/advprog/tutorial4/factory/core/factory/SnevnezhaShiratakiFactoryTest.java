package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiFactoryTest {
    private Class<?> snevnezhaShiratakiFactoryClass;
    private SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory");
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiFactoryClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideMakeFlavorMethod() throws Exception {
        Method makeFlavor = snevnezhaShiratakiFactoryClass.getDeclaredMethod("makeFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor",
                makeFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                makeFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(makeFlavor.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryReturnsRightFlavor() {
        Flavor flavor = snevnezhaShiratakiFactory.makeFlavor();
        assertEquals(flavor.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Umami");
        assertEquals(flavor.getDescription(), "Adding WanPlus Specialty MSG flavoring...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryReturnsRightMeat() {
        Meat meat = snevnezhaShiratakiFactory.makeMeat();
        assertEquals(meat.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Fish");
        assertEquals(meat.getDescription(), "Adding Zhangyun Salmon Fish Meat...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryReturnsRightNoodle() {
        Noodle noodle = snevnezhaShiratakiFactory.makeNoodle();
        assertEquals(noodle.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Shirataki");
        assertEquals(noodle.getDescription(), "Adding Snevnezha Shirataki Noodles...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryReturnsRightTopping() {
        Topping topping = snevnezhaShiratakiFactory.makeTopping();
        assertEquals(topping.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Flower");
        assertEquals(topping.getDescription(), "Adding Xinqin Flower Topping...");
    }

}
