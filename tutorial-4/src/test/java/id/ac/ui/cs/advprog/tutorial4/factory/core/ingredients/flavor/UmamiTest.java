package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class UmamiTest {
    private Class<?> umamiClass;
    private Umami umami;

    @BeforeEach
    public void setUp() throws Exception {
        umamiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Umami");
        umami = new Umami();
    }

    @Test
    public void testUmamiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(umamiClass.getModifiers()));
    }

    @Test
    public void testUmamiIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(umamiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor")));
    }

    @Test
    public void testUmamiOverrideFlavorMethod() throws Exception {
        Method getDescription = umamiClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testUmamiReturnsRightDescription() {
        String umamiResult = umami.getDescription();
        assertEquals(umamiResult.getClass().getName(),
                "java.lang.String");
        assertEquals(umamiResult, "Adding WanPlus Specialty MSG flavoring...");
    }

}
