package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @Mock
    MenuRepository menuRepository;

    @Mock
    MenuFactory menuFactory;

    @InjectMocks
    MenuService menuService = new MenuServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        ParameterizedType pt = (ParameterizedType) getMenus.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(Menu.class));
    }

    @Test
    public void testMenuServiceHasCreateMenuMethod() throws Exception {
        Class <?>[] menuArgs = new Class[2];
        menuArgs[0] = String.class;
        menuArgs[1] = String.class;
        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", menuArgs);
        int methodModifiers = createMenu.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(createMenu.getParameterCount(), 2);
        assertEquals(createMenu.getGenericReturnType(), Menu.class);
    }

    @Test
    public void testMenuServiceCreatesMenuCorrectly() throws ClassNotFoundException {
        Menu liyuanReturn = new LiyuanSoba("Sobaku");
        when(menuFactory.makeMenu(anyString(), anyString())).thenReturn(liyuanReturn);
        when(menuRepository.add(liyuanReturn)).thenReturn(liyuanReturn);
        Menu liyuanSoba = menuService.createMenu("Sobaku", "LiyuanSoba");
        assertEquals(liyuanSoba.getName(), "Sobaku");
        assertEquals(liyuanSoba.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        verify(menuRepository, times(1)).add(liyuanSoba);
    }


    @Test
    public void testMenuServiceCreatesMenuNotFoundThrowsException() throws ClassNotFoundException {
        when(menuFactory.makeMenu(anyString(), anyString())).thenThrow(new ClassNotFoundException());
        Menu liyuanSoba = menuService.createMenu("Venti", "Crot");
        assertNull(liyuanSoba);
        verify(menuRepository, never()).add(any());
    }


    @Test
    public void testMenuServiceCreatesGetMenusReturnsCorrectly() throws ClassNotFoundException {
        List<Menu> menuList = new ArrayList<>();
        when(menuRepository.getMenus()).thenReturn(menuList);
        assertSame(menuService.getMenus(), menuList);
        verify(menuRepository, times(1)).getMenus();
    }

}
