package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IngredientsFactoryTest {
    private Class<?> ingredientsFactoryClass;

    /**
     * setUp the class Object
     * @throws ClassNotFoundException self-explanatory
     */
    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        ingredientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory");
    }

    @Test
    public void testIngredientsFactoryIsAPublicInterface() {
        int classModifiers = ingredientsFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testIngredientsFactoryHasMakeFlavorAbstractMethod() throws Exception {
        Method makeFlavor = ingredientsFactoryClass.getDeclaredMethod("makeFlavor");
        int methodModifiers = makeFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, makeFlavor.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasMakeMeatAbstractMethod() throws Exception {
        Method makeMeat = ingredientsFactoryClass.getDeclaredMethod("makeMeat");
        int methodModifiers = makeMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, makeMeat.getParameterCount());
    }


    @Test
    public void testIngredientsFactoryHasMakeNoodleAbstractMethod() throws Exception {
        Method makeNoodle = ingredientsFactoryClass.getDeclaredMethod("makeNoodle");
        int methodModifiers = makeNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, makeNoodle.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasMakeToppingAbstractMethod() throws Exception {
        Method makeTopping = ingredientsFactoryClass.getDeclaredMethod("makeTopping");
        int methodModifiers = makeTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, makeTopping.getParameterCount());
    }

}
