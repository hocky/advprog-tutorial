package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class MenuRepositoryTest {
    private Class<?> menuRepositoryClass;
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() throws Exception {
        menuRepository = new MenuRepositoryImpl();
    }

    @Test
    public void testMenuRepositoryImplReturnsRightAttributes() {
        Menu liyuanSoba = new LiyuanSoba("Soba gurih");
        Menu mondoUdon = new MondoUdon("Udon enak");
        menuRepository.add(liyuanSoba);
        menuRepository.add(mondoUdon);
        List<Menu> menuList = menuRepository.getMenus();
        assertTrue(menuList.contains(liyuanSoba));
        assertTrue(menuList.contains(mondoUdon));
        assertEquals(menuList.size(), 2);
    }
}
