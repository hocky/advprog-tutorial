package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NoodleTest {
    private Class<?> NoodleClass;

    /**
     * setUp the class Object
     * @throws ClassNotFoundException self-explanatory
     */
    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        NoodleClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle");
    }

    @Test
    public void testNoodleIsAPublicInterface() {
        int classModifiers = NoodleClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testNoodleHasMakeNoodleAbstractMethod() throws Exception {
        Method getDescription = NoodleClass.getDeclaredMethod("getDescription");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }
}
