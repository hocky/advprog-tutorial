package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class CheeseTest {
    private Class<?> cheeseClass;
    private Cheese cheese;

    @BeforeEach
    public void setUp() throws Exception {
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Cheese");
        cheese = new Cheese();
    }

    @Test
    public void testCheeseIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(cheeseClass.getModifiers()));
    }

    @Test
    public void testCheeseIsATopping() {
        Collection<Type> interfaces = Arrays.asList(cheeseClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping")));
    }

    @Test
    public void testCheeseOverrideToppingMethod() throws Exception {
        Method getDescription = cheeseClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testCheeseReturnsRightDescription() {
        String cheeseResult = cheese.getDescription();
        assertEquals(cheeseResult.getClass().getName(),
                "java.lang.String");
        assertEquals(cheeseResult, "Adding Shredded Cheese Topping...");
    }

}
