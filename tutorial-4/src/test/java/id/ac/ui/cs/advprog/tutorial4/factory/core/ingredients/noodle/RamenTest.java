package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class RamenTest {
    private Class<?> ramenClass;
    private Ramen ramen;

    @BeforeEach
    public void setUp() throws Exception {
        ramenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Ramen");
        ramen = new Ramen();
    }

    @Test
    public void testRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(ramenClass.getModifiers()));
    }

    @Test
    public void testRamenIsANoodle() {
        Collection<Type> interfaces = Arrays.asList(ramenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle")));
    }

    @Test
    public void testRamenOverrideNoodleMethod() throws Exception {
        Method getDescription = ramenClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testRamenReturnsRightDescription() {
        String ramenResult = ramen.getDescription();
        assertEquals(ramenResult.getClass().getName(),
                "java.lang.String");
        assertEquals(ramenResult, "Adding Inuzuma Ramen Noodles...");
    }

}
