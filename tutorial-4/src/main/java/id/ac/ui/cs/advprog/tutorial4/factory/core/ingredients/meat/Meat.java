package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

public interface Meat {
    public String getDescription();
}