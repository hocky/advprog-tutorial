package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

public interface Noodle {
    public String getDescription();
}