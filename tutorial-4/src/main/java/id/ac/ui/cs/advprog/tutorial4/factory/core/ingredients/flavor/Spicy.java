package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

public class Spicy implements Flavor {
    public String getDescription(){
        return "Adding Liyuan Chili Powder...";
    }
}