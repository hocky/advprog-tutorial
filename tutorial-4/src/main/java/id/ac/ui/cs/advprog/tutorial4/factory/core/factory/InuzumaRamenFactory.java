package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;

public class InuzumaRamenFactory implements IngredientsFactory {
    @Override
    public Flavor makeFlavor() {
        return new Spicy();
    }

    @Override
    public Meat makeMeat() {
        return new Pork();
    }

    @Override
    public Noodle makeNoodle() {
        return new Ramen();
    }

    @Override
    public Topping makeTopping() {
        return new BoiledEgg();
    }
}
