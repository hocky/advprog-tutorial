package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;

/**
 * Menu untuk setiap macam jenis Mie
 */
public abstract class Menu {
    private String name;
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;
    protected IngredientsFactory ingredientsFactory;

    public Menu(String name){
        this.name = name;
    }

    public void prepare(){
        noodle = ingredientsFactory.makeNoodle();
        meat = ingredientsFactory.makeMeat();
        topping = ingredientsFactory.makeTopping();
        flavor = ingredientsFactory.makeFlavor();
    }

    public String getName() {
        return name;
    }

    public Noodle getNoodle() {
        return noodle;
    }

    public Meat getMeat() {
        return meat;
    }

    public Topping getTopping() {
        return topping;
    }

    public Flavor getFlavor() {
        return flavor;
    }
}