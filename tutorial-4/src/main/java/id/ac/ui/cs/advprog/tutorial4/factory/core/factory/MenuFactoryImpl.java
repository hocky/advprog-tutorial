package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import org.springframework.stereotype.Component;

@Component
public class MenuFactoryImpl implements MenuFactory {

    @Override
    public Menu makeMenu(String name, String type) throws ClassNotFoundException {
        IngredientsFactory ingredientsFactory;
        switch (type) {
            case ("LiyuanSoba"):
                return new LiyuanSoba(name);
            case ("InuzumaRamen"):
                return new InuzumaRamen(name);
            case ("MondoUdon"):
                return new MondoUdon(name);
            case ("SnevnezhaShirataki"):
                return new SnevnezhaShirataki(name);
        }
        throw new ClassNotFoundException("Wanted menu doesn't found");
    }
}
