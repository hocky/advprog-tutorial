package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private MenuFactory menuFactory;

    public Menu createMenu(String name, String type) {
        try {
            return menuRepository.add(menuFactory.makeMenu(name, type));
        } catch (Exception e){
            return null;
        }
    }

    public List<Menu> getMenus() {
        return menuRepository.getMenus();
    }

}