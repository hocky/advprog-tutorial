package id.ac.ui.cs.advprog.tutorial4.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AbstractFactoryInitializer {
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private MenuFactory menuFactory;
    @PostConstruct
    public void init() throws ClassNotFoundException {
        menuRepository.add(menuFactory.makeMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba"));
        menuRepository.add(menuFactory.makeMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen"));
        menuRepository.add(menuFactory.makeMenu("Good Hunter Cheese Chicken Udon", "MondoUdon"));
        menuRepository.add(menuFactory.makeMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki"));
    }
}
