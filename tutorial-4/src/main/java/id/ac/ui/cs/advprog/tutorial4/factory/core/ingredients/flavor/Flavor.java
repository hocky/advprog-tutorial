package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

public interface Flavor {
    public String getDescription();
}