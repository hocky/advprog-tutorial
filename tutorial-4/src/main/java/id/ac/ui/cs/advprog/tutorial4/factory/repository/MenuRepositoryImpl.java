package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.ArrayList;

@Repository
public class MenuRepositoryImpl implements MenuRepository {
    private List<Menu> menuList;

    public MenuRepositoryImpl(){
        this.menuList = new ArrayList<>();
    }

    public List<Menu> getMenus(){
        return menuList;
    }

    public Menu add(Menu menu){
        menuList.add(menu);
        return menu;
    }
}