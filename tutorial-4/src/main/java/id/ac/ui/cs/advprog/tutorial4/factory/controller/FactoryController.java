package id.ac.ui.cs.advprog.tutorial4.factory.controller;

import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class FactoryController {
    @Autowired
    private MenuService menuService;

    /**
     * Controller yang menangani request membuka menu
     * @param model model yang bersangkutan
     * @return halaman views yang digunakan
     */
    @GetMapping("/menus")
    public String getmenus(Model model){
        model.addAttribute("RestaurantMenu", menuService.getMenus());
        return "factory/menus";
    }

    /**
     * Controller yang menangani request POST
     * @param request https://stackoverflow.com/questions/39136164/
     *                whats-the-difference-usage-of-model-and-httpservletrequest-in-springmvccan-htt
     * @return redirection to a certain pages
     */
    @PostMapping("/newMenu")
    public String createMenu(HttpServletRequest request){
        String name = request.getParameter("name");
        String type = request.getParameter("type");
        menuService.createMenu(name, type);
        return "redirect:/menus";
    }
}