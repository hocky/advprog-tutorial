package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

public class Beef implements Meat {
    public String getDescription(){
        return "Adding Maro Beef Meat...";
    }
}