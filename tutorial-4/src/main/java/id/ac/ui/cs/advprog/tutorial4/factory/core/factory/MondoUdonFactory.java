package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;

public class MondoUdonFactory implements IngredientsFactory {
    @Override
    public Flavor makeFlavor() {
        return new Salty();
    }

    @Override
    public Meat makeMeat() {
        return new Chicken();
    }

    @Override
    public Noodle makeNoodle() {
        return new Udon();
    }

    @Override
    public Topping makeTopping() {
        return new Cheese();
    }
}
