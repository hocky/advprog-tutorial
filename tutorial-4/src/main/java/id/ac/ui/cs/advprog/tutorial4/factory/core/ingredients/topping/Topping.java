package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

public interface Topping {
    public String getDescription();
}