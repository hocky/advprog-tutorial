package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

public class Chicken implements Meat {
    public String getDescription(){
        return "Adding Wintervale Chicken Meat...";
    }
}