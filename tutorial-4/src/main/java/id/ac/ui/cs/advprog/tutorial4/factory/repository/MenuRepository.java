package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import java.util.List;

public interface MenuRepository {
    List<Menu> getMenus();
    Menu add(Menu menu);
}
