package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.springframework.context.annotation.Bean;

public interface MenuFactory {
    Menu makeMenu(String name, String type) throws ClassNotFoundException;
}
