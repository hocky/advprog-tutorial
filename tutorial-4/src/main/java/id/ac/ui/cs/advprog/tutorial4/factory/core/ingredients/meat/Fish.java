package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

public class Fish implements Meat {
    public String getDescription(){
        return "Adding Zhangyun Salmon Fish Meat...";
    }
}