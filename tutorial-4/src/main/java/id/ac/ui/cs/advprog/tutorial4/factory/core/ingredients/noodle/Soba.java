package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

public class Soba implements Noodle {
    public String getDescription(){
        return "Adding Liyuan Soba Noodles...";
    }
}