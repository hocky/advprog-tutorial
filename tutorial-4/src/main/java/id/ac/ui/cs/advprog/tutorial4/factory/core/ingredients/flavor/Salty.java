package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

public class Salty implements Flavor {
    public String getDescription(){
        return "Adding a pinch of salt...";
    }
}