package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;

public class LiyuanSobaFactory implements IngredientsFactory {
    @Override
    public Flavor makeFlavor() {
        return new Sweet();
    }

    @Override
    public Meat makeMeat() {
        return new Beef();
    }

    @Override
    public Noodle makeNoodle() {
        return new Soba();
    }

    @Override
    public Topping makeTopping() {
        return new Mushroom();
    }
}
