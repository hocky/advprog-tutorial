package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

public class Shirataki implements Noodle {
    public String getDescription(){
        return "Adding Snevnezha Shirataki Noodles...";
    }
}