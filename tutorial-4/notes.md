## Notes

### Lazy Instantiation

*Lazy instantiation* pada sebuah objek singleton maknanya ialah pembuatan dari objek tersebut hanya dilakukan saat pertama kali dia kan digunakan, dalam konteks ini, ialah pertama kali dilakukan pemanggilan `getInstance()`, perhatikan bahwa biasanya ini digunakan untuk meningkatkan performa atau kinerja, menghindari perhitungan atau tenaga komputasi yang boros, mengurangi penggunaan program memory.

Biasanya ini itu terjadi saat kasus dimana pembuatan objek lumayan mahal. Dalam kasus ini mungkin saja misalkan yang ingin memesan minuman/makanan jarang sekali. Daripada kita terus-terusan memaintain objek sejak pertama kali dijalankan, ini bisa mengambil resource penggunaan memori yang lumayan. Misalnya dalam konteks penggunaan database yang besar atau penampungan array atau data struktur yang besar.

Keuntungan:

- Menghemat penggunaan memori dan overhead.
- Run-time awal bisa lebih lancar.

Kerugian:

- Agak sulit menangani pada kasus multithreading pemanggilan `getInstance()` bersamaan.
- Apabila pemanggilan `getInstance()` sering, maka akan ada pengecekan apakah objek sudah ada atau belum berkali-kali.

## Eager Instantiation

Kebalikan dari *lazy*, objek dari Singleton akan dibuat dari awal dan setiap kali `getInstance()` dipanggil, pasti akan langsung mengembalikan objek tersebut.

Keuntungan:

- Multithreading bisa ditangani dengan mudah, karena pasti sudah pasti mengembalikan objek yang sama dari awal objek tersebut dibuat.
- Pemanggilan `getInstance()` yang kentara dan sering dilakukan akan sangat cocok.

Kerugian:

- Apabila singleton banyak, maka dari awal overhead akan mahal.
- Apabila jarang-jarang digunakan, maka dari awal sudah menggunakan resource yang banyak.