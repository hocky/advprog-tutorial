package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "🛡🛡 You can't break through my shield!";
    }

    @Override
    public String getType() {
        return "Shield";
    }
}
