package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    
    @Override
    public String attack() {
        return "🧙🏻‍♂️✨ The brightest flame casts the darkest shadow.";
    }

    @Override
    public String getType() {
        return "Magic";
    }
}
