package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {

    private final QuestRepository questRepository;
    private final Guild guild;
    private final Adventurer agileAdventurer;
    private final Adventurer knightAdventurer;
    private final Adventurer mysticAdventurer;

    public GuildServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
        this.guild = new Guild();
        guild.add(this.agileAdventurer = new AgileAdventurer(guild));
        guild.add(this.knightAdventurer = new KnightAdventurer(guild));
        guild.add(this.mysticAdventurer = new MysticAdventurer(guild));
    }

    @Override
    public List<Adventurer> getAdventurers() {
        return guild.getAdventurers();
    }

    @Override
    public void addQuest(Quest quest) {
        guild.addQuest(quest);
        questRepository.save(quest);
        return;
    }
}
