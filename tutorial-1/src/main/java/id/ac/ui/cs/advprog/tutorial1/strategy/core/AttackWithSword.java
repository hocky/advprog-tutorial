package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    @Override
    public String attack() {
        return "⚔🩸 A sword wields no strength, unless the hand that holds it has courage.";
    }

    @Override
    public String getType() {
        return "Sword";
    }
}
