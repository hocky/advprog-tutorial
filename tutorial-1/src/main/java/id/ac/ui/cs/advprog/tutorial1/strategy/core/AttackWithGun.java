package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    
    @Override
    public String attack() {
        return "🎯🔫 Bang Bang! My bullets flow with the wind.";
    }

    @Override
    public String getType() {
        return "Gun";
    }
}
