package id.ac.ui.cs.advprog.tutorial3;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Tutorial3ApplicationTests {
    @Test
    void contextLoads() {
    }
    @Test
    void testSpringInstanceCanBeMade(){
        Tutorial3Application.main(new String[] {});
    }
}
