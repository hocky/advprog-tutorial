package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FacadeTransformationTest {
    private Class<?> FacadeClass;
    private final String ALPHA_STR = "Password facebook Hocky ialah VentyWangiHuhaHuha12342069";

    @BeforeEach
    public void setup() throws ClassNotFoundException {
        FacadeClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.FacadeTransformation");
    }

    @Test
    public void testFacadeHasEncodeMethod() throws NoSuchMethodException {
        Method translate = FacadeClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeEncodesCorrectly(){
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "wFMD*sXddv/AZ}x$V@$@npMp;n%dA/HbvM>q+C%v(nCCZyqx$AVA";
        Spell result = new FacadeTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testFacadeHasDecodeMethod() throws NoSuchMethodException {
        Method translate = FacadeClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeDecodesCorrectly(){
        String text = "wFMD*sXddv/AZ}x$V@$@npMp;n%dA/HbvM>q+C%v(nCCZyqx$AVA";
        Codex codex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new FacadeTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testFacadeTransformationCanReceiveNoEncoderAndDecoder(){
        List<Transformation> transformationList = new ArrayList<>();
        FacadeTransformation facadeTransformation = new FacadeTransformation(transformationList);
        String codexTranslation = facadeTransformation.encode(new Spell(ALPHA_STR, AlphaCodex.getInstance())).getText();
        assertEquals(ALPHA_STR, facadeTransformation.decode(new Spell(codexTranslation, RunicCodex.getInstance())).getText());
        assertEquals("+JNNDXBz_cJLxKXXa_,XLaG_nJsJb_yxZMGuJZvn,AbJ,AbJ!@#$@)^(", codexTranslation);
    }
    @Test
    public void testFacadeTransformationCanReceiveMultipleEncoderAndDecoder(){
        List<Transformation> transformationList = new ArrayList<>();
        transformationList.add(new CaesarTransformation(5));
        transformationList.add(new CelestialTransformation());
        transformationList.add(new ToneDeafBardTransformation());
        transformationList.add(new AbyssalTransformation(-3));
        transformationList.add(new CaesarTransformation(14));
        transformationList.add(new CelestialTransformation("AkiraHuHa"));
        FacadeTransformation facadeTransformation = new FacadeTransformation(transformationList);
        String codexTranslation = facadeTransformation.encode(new Spell(ALPHA_STR, AlphaCodex.getInstance())).getText();
        assertEquals(ALPHA_STR, facadeTransformation.decode(new Spell(codexTranslation, RunicCodex.getInstance())).getText());
        assertEquals("y.$%DFSq)Vc@w+&B_wy@)X,vd^vZz^|zSFz*_!SFi)C@$#mb!N/KV&y[", codexTranslation);
    }

}
