package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ToneDeafBardTransformationTest {
    private Class<?> toneDeafBardClass;

    @BeforeEach
    public void setup() throws ClassNotFoundException {
        toneDeafBardClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.ToneDeafBardTransformation");
    }

    @Test
    public void testToneDeafBardHasEncodeMethod() throws NoSuchMethodException {
        Method translate = toneDeafBardClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testToneDeafBardEncodesCorrectly(){
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "drows ruo egrof ot htimskcalb a ot tnew I dna arifaS";

        Spell result = new ToneDeafBardTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testToneDeafBardHasDecodeMethod() throws NoSuchMethodException {
        Method translate = toneDeafBardClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testToneDeafBardDecodesCorrectly(){
        String text = "drows ruo egrof ot htimskcalb a ot tnew I dna arifaS";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new ToneDeafBardTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

}
