package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CodexTranslatorTest {

    @Spy
    AlphaCodex oldCodex;
    @Spy
    RunicCodex newCodex;

    @Test
    public void testCodexHasTranslateStaticMethod() throws Exception {
        Class<?> translatorClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator");
        Method translate = translatorClass.getDeclaredMethod("translate", Spell.class, Codex.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isStatic(methodModifiers));
    }

    @Test
    public void testCodexTranslateAlphaToRunicProperly(){
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Codex targetCodex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "eJcnBJ_JZz_._DxZM_MX_J_KsJLaNdnMb_MX_cXBvx_XAB_NDXBz";

        Spell result = CodexTranslator.translate(spell, targetCodex);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testOldAndNewCodexDifferentCharSize(){
        Spell spell = new Spell("Akira Huha huha wangy 91919", oldCodex);

        when(oldCodex.getCharSize()).thenReturn(420);
        when(newCodex.getCharSize()).thenReturn(69);
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> CodexTranslator.translate(spell, newCodex));
        assertTrue(exception.getMessage().contains("Jumlah karakter pada kedua Codex tidak sama"));
        verify(oldCodex, atLeastOnce()).getCharSize();
        verify(newCodex, atLeastOnce()).getCharSize();
    }

    @Test
    public void testMakeSingleInstanceOfCodexTranslator(){
        CodexTranslator codexTranslator = new CodexTranslator();
        assertTrue(codexTranslator instanceof CodexTranslator);
    }
}
