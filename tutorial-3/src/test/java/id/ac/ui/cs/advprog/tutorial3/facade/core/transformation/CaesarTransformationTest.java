package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CaesarTransformationTest {
    private Class<?> caesarClass;

    @BeforeEach
    public void setup() throws ClassNotFoundException {
        caesarClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation");
    }

    @Test
    public void testCaesarHasEncodeMethod() throws NoSuchMethodException {
        Method translate = caesarClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarEncodesCorrectly(){
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "MUZclU4UhX4C4qYhn4ni4U4VfUWemgcnb4ni4ZilaY4iol4mqilX";

        Spell result = new CaesarTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarEncodesCorrectlyWithCustomKey(){
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";

        Spell result = new CaesarTransformation(-5).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarHasDecodeMethod() throws NoSuchMethodException {
        Method translate = caesarClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarDecodesCorrectly(){
        String text = "MUZclU4UhX4C4qYhn4ni4U4VfUWemgcnb4ni4ZilaY4iol4mqilX";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new CaesarTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarDecodesCorrectlyWithCustomKey(){
        String text = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new CaesarTransformation(-5).decode(spell);
        assertEquals(expected, result.getText());
    }
}
