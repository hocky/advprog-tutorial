package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private final Bow bow;
    private boolean spontaneous;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.spontaneous = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(spontaneous);
    }

    @Override
    public String chargedAttack() {
        spontaneous = !spontaneous;
        return (spontaneous ? "Entered" : "Leaving") + " aim shot mode";
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
