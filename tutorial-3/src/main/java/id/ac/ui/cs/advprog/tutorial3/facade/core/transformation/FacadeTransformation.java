package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.ArrayList;
import java.util.List;

public class FacadeTransformation implements Transformation{
    private final List<Transformation> transformationList;

    public FacadeTransformation(List<Transformation> transformationList){
        this.transformationList = transformationList;
    }

    public FacadeTransformation(){
        this(new ArrayList<Transformation>(){
            {
                add(new AbyssalTransformation());
                add(new CaesarTransformation());
                add(new CelestialTransformation());
                add(new ToneDeafBardTransformation());
            }
        });
    }

    @Override
    public Spell encode(Spell spell) {
        for(Transformation transformation : transformationList){
            spell = transformation.encode(spell);
        }
        return CodexTranslator.translate(spell, RunicCodex.getInstance());
    }

    @Override
    public Spell decode(Spell spell) {
        for(int i = transformationList.size() - 1;i >= 0;i--){
            spell = transformationList.get(i).decode(spell);
        }
        return CodexTranslator.translate(spell, AlphaCodex.getInstance());
    }
}
