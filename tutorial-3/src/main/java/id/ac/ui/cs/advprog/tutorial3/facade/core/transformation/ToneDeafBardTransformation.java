package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class ToneDeafBardTransformation implements Transformation{
    @Override
    public Spell encode(Spell spell) {
        return process(spell);
    }

    @Override
    public Spell decode(Spell spell) {
        return process(spell);
    }

    public Spell process(Spell spell){
        StringBuilder currentText = new StringBuilder(spell.getText());
        currentText.reverse();
        return new Spell(currentText.toString(), spell.getCodex());
    }

}
