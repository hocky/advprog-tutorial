package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation implements Transformation{

    private final int key;

    public CaesarTransformation(int key){
        this.key = key;
    }

    public CaesarTransformation(){
        this(69);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    /**
     * Caesar cipher with shift +key
     * @param spell the spell to process
     * @param encode whether to encode or decode
     * @return
     */
    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int shift = encode ? -key : key;
        int n = text.length();
        int m = codex.getCharSize();
        char[] res = new char[n];
        for(int i = 0; i < n; i++) {
            int newIdx = (codex.getIndex(text.charAt(i)) + shift) % m;
            newIdx = newIdx < 0 ? m + newIdx : newIdx;
            res[i] = codex.getChar(newIdx);
        }
        return new Spell(new String(res), codex);
    }
}
