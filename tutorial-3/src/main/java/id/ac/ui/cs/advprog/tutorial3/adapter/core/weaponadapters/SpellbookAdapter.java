package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private final Spellbook spellbook;
    private boolean isLastLarge;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        isLastLarge = false;
    }

    @Override
    public String normalAttack() {
        isLastLarge = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        isLastLarge = !isLastLarge;
        return isLastLarge ? spellbook.largeSpell() : "❌❌❌ Can't cast large spell twice in a row";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
