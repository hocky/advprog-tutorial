package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;

    @Override
    public List<Weapon> findAll() {
        
        List<Weapon> allWeapon = weaponRepository.findAll();

        for(Bow bow : bowRepository.findAll())
            if(weaponRepository.findByAlias(bow.getName()) == null)
                allWeapon.add(new BowAdapter(bow));

        for(Spellbook spellbook : spellbookRepository.findAll())
            if(weaponRepository.findByAlias(spellbook.getName()) == null)
                allWeapon.add(new SpellbookAdapter(spellbook));
        
        return allWeapon;
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon;
        weapon = weaponRepository.findByAlias(weaponName);
        if(weapon == null){
            Bow currentBow = bowRepository.findByAlias(weaponName);
            if(currentBow != null) weapon = new BowAdapter(currentBow);
        }
        if(weapon == null){
            weapon = new SpellbookAdapter(spellbookRepository.findByAlias(weaponName));
        }
        logRepository.addLog(String.format("%s attacked with %s (%s attack): %s",
                weapon.getHolderName(),
                weapon.getName(),
                attackType == 0 ? "normal" : "charged",
                attackType == 0 ? weapon.normalAttack() : weapon.chargedAttack()
        ));
        // Melakukan save pada weapon repository
        weaponRepository.save(weapon);

        return;
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
