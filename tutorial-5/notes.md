

 ## Requirements

Pertama, kita bisa membuat ERD-nya.

<img src="notes.assets/image-20210405212055385.png" alt="image-20210403170745824" style="zoom: 50%;" />

Selanjutnya, kita bisa mencatat kebutuhan apa saja yang kita inginkan.

- Mahasiswa

  - ✔ Membuat Instance Mahasiswa
  - ✔ Mengembalikan semua instance mahasiswa
  - ✔ Mengganti NPM Mahasiswa
  - ✔ Menghapus Mahasiswa
  - ✔ Mengembalikan sebuah instance mahasiswa

![image-20210405150237934](notes.assets/image-20210405150237934.png)

- Mata Kuliah
  - ✔ Mendaftar semua mata kuliah
  - ✔ Membuat Instance mata kuliah
  - ✔ Mengecek sebuah Instance mata kuliah
  - ✔ Mengambil instance mata kuliah dengan kode tertentu
  - ✔ Mengganti Kode mata kuliah

![image-20210405150255265](notes.assets/image-20210405150255265.png)

- Log (Semuanya sudah dilengkapi dengan autorisasi JWT)
  - Mulai mengisi log (Harus register terlebih dahulu) (GET)
  
    ```
    http://localhost:8080/log/register/[kodeMatkul]/
    ```
  
  - Membuat Log baru (Diimplementasikan di log) (POST) dengan payload body Log

    ```
    http://localhost:8080/log
    ```
  
  - Menghapus log yang sudah ada (Diimplementasikan di log) (DELETE)
  
    ```
    http://localhost:8080/log/[kodeLog]
    ```
  
  - Mengedit log yang sudah ada (PUT) dengan payload log
  
    ```
    http://localhost:8080/log/[kodeLog]
    ```
  
  - Mengecek log dengan kode tertentu (GET)
  
    ```
    http://localhost:8080/log/[kodeLog]
    ```
  
  - Mengecek laporan pembayaran bulan tertentu (GET), akan dikembalikan semua laporan dan juga summarynya.
  
    ```
    http://localhost:8080/log/summary/[BulanInEnglish]
    ```
  
  - Mengecek summary Jumlah uang yang didapat dengan summary kurang lebih sebagai berikut. (GET)
  
    ```
    http://localhost:8080/log/summary
    ```
    
    ```json
    [
        {
            "payRate": 350,
            "month": "February",
            "jamKerja": 2162.016666666667,
            "pembayaran": 756705
        },
        {
            "payRate": 350,
            "month": "March",
            "jamKerja": 1418.0166666666667,
            "pembayaran": 496305
        },
        {
            "payRate": 350,
            "month": "May",
            "jamKerja": 2.0166666666666666,
            "pembayaran": 705
        }
    ]
    ```

![image-20210405150151568](notes.assets/image-20210405150151568.png)

Setelah itu, akan kita coba implementasikan juga nantinya untuk fitur autentikasi.


## Extra

![image-20210403161642997](notes.assets/image-20210403161642997.png)

Berikut ialah contoh untuk cara menggunakan Postman.

Bisa masukkan raw data pada request body dengan isinya sesuai dengan atribut model yang direquest pada setiap endpoints POST.

Untuk cara menggunakan relationshipnya bisa dilihat dari sini.

https://www.baeldung.com/jpa-join-column

The annotation *javax.persistence.JoinColumn* marks a column for as a join column for an entity association or an element collection.

### *@OneToMany* Mapping Example

When using a *@OneToMany* mapping we can use the *mappedBy* parameter to indicate that the given column is owned by another entity.

```java
@Entity
public class Employee {
 
    @Id
    private Long id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Email> emails;
}

@Entity
public class Email {
 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;
}
```

In the above example, *Email* (the owner entity) has a join column *employee_id* that stores the id value and has a foreign key to the *Employee* entity.

![ ](notes.assets/joincol1.png)

https://www.springboottutorial.com/unit-testing-for-spring-boot-rest-services