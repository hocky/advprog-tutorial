package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.TreeMap;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    private Log authorizeLogFacade(int kodeLog){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String npm = authentication.getName();

        Log log = logService.getLog(kodeLog);

        return log.getMahasiswa().getNpm().equals(npm) ? log : null;

    }

    @GetMapping(path = "register/{kodeMatkul}", produces = {"application/json"})
    public ResponseEntity<Mahasiswa>registerToMataKuliah(@PathVariable(value = "kodeMatkul")  String kodeMatkul) throws JsonProcessingException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String npm = authentication.getName();

        Mahasiswa mahasiswa = logService.registerToMataKuliah(npm, kodeMatkul);
        return ResponseEntity.ok(mahasiswa);

    }

    @PostMapping(path = "", produces = {"application/json"})
    public ResponseEntity<Log> createLog(@RequestBody Log log){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String npm = authentication.getName();

        log = logService.createLog(npm, log);
        return ResponseEntity.ok(log);
    }

    @DeleteMapping(path = "/{kodeLog}", produces = {"application/json"})
    public ResponseEntity<HttpStatus> deleteLogByKodeLog(@PathVariable(value = "kodeLog") int kodeLog){

        Log log = authorizeLogFacade(kodeLog);
        if(log == null) return new ResponseEntity(HttpStatus.FORBIDDEN);

        int status = logService.deleteLogByKodeLog(kodeLog);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "summary/{month}", produces = {"application/json"})
    public ResponseEntity<Map<String, Object>> getLogOfMahasiswa(@PathVariable(value = "month") String month){



        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String npm = authentication.getName();
        Map<String, Object> returnResult = new TreeMap<>();

        returnResult.put("summary", logService.getLogOfMahasiswa(npm, month));
        returnResult.put("daftarLog", logService.getLogByWaktuMulai(npm, month));
        return ResponseEntity.ok(returnResult);
    }

    @GetMapping(path = "summary", produces = {"application/json"})
    public ResponseEntity<Iterable<LogSummary>> getSummary(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String npm = authentication.getName();

        Iterable<LogSummary> logSummaries = logService.getLogSummary(npm);
        return ResponseEntity.ok(logSummaries);
    }

    @GetMapping(path = "/{kodeLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "kodeLog") int kodeLog) {
        Log log = authorizeLogFacade(kodeLog);
        return (log == null ? new ResponseEntity(HttpStatus.FORBIDDEN) : ResponseEntity.ok(log));
    }

    @PutMapping(path = "/{kodeLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "kodeLog") int kodeLog,
                                    @RequestBody Log currentLog) {

        Log log = authorizeLogFacade(kodeLog);
        if(log == null) return new ResponseEntity(HttpStatus.FORBIDDEN);

        return (currentLog.getDurasiKerja() < 0 ? new ResponseEntity(HttpStatus.BAD_REQUEST):
                ResponseEntity.ok(logService.updateLog(kodeLog, log)));
    }
}
