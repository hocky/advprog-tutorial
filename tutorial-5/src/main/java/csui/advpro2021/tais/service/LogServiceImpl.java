package csui.advpro2021.tais.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl implements LogService{
    @Autowired
    MahasiswaService mahasiswaService;

    @Autowired
    MataKuliahService mataKuliahService;

    @Autowired
    LogRepository logRepository;

    @Autowired
    MahasiswaRepository mahasiswaRepository;

    @Autowired
    MataKuliahRepository mataKuliahRepository;

    @Override
    public Mahasiswa registerToMataKuliah(String npm, String kodeMatkul) throws JsonProcessingException {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        MataKuliah mataKuliah = mataKuliahService.getMataKuliah(kodeMatkul);

        mahasiswa.setMataKuliah(mataKuliah);
        mataKuliah.getAsdos().add(mahasiswa);

        mahasiswaRepository.save(mahasiswa);
        mataKuliahRepository.save(mataKuliah);

        return mahasiswa;
    }

    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);

        log.setMahasiswa(mahasiswa);
        mahasiswa.getLogMahasiswa().add(log);

        mahasiswaRepository.save(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLog(int kodeLog) {
        return logRepository.findByKodeLog(kodeLog);
    }

    @Override
    public int deleteLogByKodeLog(int kodeLog) {
        logRepository.deleteById(kodeLog);
        return 200;
    }

    @Override
    public LogSummary getLogOfMahasiswa(String npm, String month) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        return LogSummary.getMonthlyLog(mahasiswa.getLogMahasiswa(), month);
    }

    @Override
    public Iterable<LogSummary> getLogSummary(String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        return LogSummary.getAllLog(mahasiswa.getLogMahasiswa());
    }

    @Override
    public Log updateLog(int kodeLog, Log log) {
        Log currentLog = getLog(kodeLog);
        currentLog.setDeskripsi(log.getDeskripsi());
        currentLog.setWaktuMulai(log.getWaktuMulai());
        currentLog.setWaktuAkhir(log.getWaktuAkhir());
        logRepository.save(currentLog);
        return currentLog;
    }

    @Override
    public Iterable<Log> getLogByWaktuMulai(String npm, String month) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        return LogSummary.getFilteredLog(mahasiswa.getLogMahasiswa(), month);
    }

}
