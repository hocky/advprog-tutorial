package csui.advpro2021.tais.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;

public interface LogService {
    Mahasiswa registerToMataKuliah(String npm, String kodeMatkul) throws JsonProcessingException;

    Log createLog(String npm, Log log);

    Log getLog(int kodeLog);

    int deleteLogByKodeLog(int kodeLog);

    LogSummary getLogOfMahasiswa(String npm, String month);

    Iterable<LogSummary> getLogSummary(String npm);

    Log updateLog(int kodeLog, Log log);

    Iterable<Log> getLogByWaktuMulai(String npm, String month);
}
