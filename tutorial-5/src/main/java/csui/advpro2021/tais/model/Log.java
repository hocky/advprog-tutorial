package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "Log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int kodeLog;

    @Column(name="deskripsi")
    private String deskripsi;

    @Column(name="waktu_mulai")
    @JsonFormat(pattern="dd-MM-yyyy HH:mm")
    private LocalDateTime waktuMulai;

    @Column(name="waktu_akhir")
    @JsonFormat(pattern="dd-MM-yyyy HH:mm")
    private LocalDateTime waktuAkhir;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="log_mahasiswa")
    private Mahasiswa mahasiswa;

    public long getDurasiKerja(){
        return Duration.between(waktuMulai, waktuAkhir).toMinutes();
    }

    public Log(String deskripsi, LocalDateTime waktuMulai, LocalDateTime waktuAkhir){
        this.deskripsi = deskripsi;
        this.waktuMulai = waktuMulai;
        this.waktuAkhir = waktuMulai.isBefore(waktuAkhir) ? waktuAkhir : waktuMulai;
    }

    public Log(String deskripsi, String waktuMulai, String waktuAkhir){
        this(
                deskripsi,
                LocalDateTime.parse(waktuMulai, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")),
                LocalDateTime.parse(waktuAkhir, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"))
        );
    }

}
