package csui.advpro2021.tais.model;

import lombok.Data;

import java.time.Duration;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

@Data
public class LogSummary {

    private final long payRate;

    private String month;

    private double jamKerja;

    private long pembayaran;

    public LogSummary(String month, double jamKerja){
        this(month, jamKerja, 350);
    }

    public LogSummary(String month, double jamKerja, long payRate){
        this.payRate = payRate;
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = (long) (payRate * jamKerja);
    }

    public static LogSummary getMonthlyLog(Iterable<Log> logList, String month){
        double sumMenitKerja = 0;
        for(Log logEntry : logList){
            if(logEntry.getWaktuMulai().getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equals(month)){
                sumMenitKerja += logEntry.getDurasiKerja();
            }
        }
        Calendar cal = Calendar.getInstance();
        return new LogSummary(month,sumMenitKerja/60);
    }

    public static Iterable<LogSummary> getAllLog(Iterable<Log> logList){
        double[] sumMenitKerja = new double[12];
        for(Log logEntry : logList){
            int currentMonth = logEntry.getWaktuMulai().getMonthValue() - 1;
            sumMenitKerja[currentMonth] += logEntry.getDurasiKerja();
        }
        Calendar cal = Calendar.getInstance();
        List<LogSummary> logSummary = new ArrayList<>();
        for(int i = 0;i < 12;i++){
            cal.set(Calendar.MONTH, i);
            String monthName = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
            if(sumMenitKerja[i] == 0) continue;
            logSummary.add(new LogSummary(monthName, sumMenitKerja[i]/60.0));
        }
        return logSummary;
    }

    public static Iterable<Log> getFilteredLog(Iterable<Log> logList, String month){

        List<Log> filteredLog = new ArrayList<>();
        for(Log logEntry : logList){
            if(logEntry.getWaktuMulai().getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equals(month)){
                filteredLog.add(logEntry);
            }
        }

        return filteredLog;
    }
}
