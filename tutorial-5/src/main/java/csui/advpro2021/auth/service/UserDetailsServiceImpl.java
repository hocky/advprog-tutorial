package csui.advpro2021.auth.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    MahasiswaService mahasiswaService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(username);
        if(mahasiswa == null){
            throw new UsernameNotFoundException("\"User not found with username: \" + username");
        }else{
            // Last parameter is authority
            return new User(mahasiswa.getNpm()
                    , mahasiswa.getPassword()
                    , new ArrayList<>());
        }
    }
}
