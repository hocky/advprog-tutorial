package csui.advpro2021.auth.controller;

import csui.advpro2021.auth.model.JwtRequest;
import csui.advpro2021.auth.model.JwtResponse;
import csui.advpro2021.auth.service.UserDetailsServiceImpl;
import csui.advpro2021.auth.utility.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HomeController {

    @Autowired
    private JWTUtility jwtUtility;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @GetMapping(value = "/check", produces = {"application/json"})
    public ResponseEntity<Map<String, Object>> home(
            @RequestHeader(value = "Authorization", required = false) String authstr){

        String username = jwtUtility.getUsernameFromAuthorizationString(authstr);
        Map <String, Object> returnStatus = new HashMap<>();

        returnStatus.put("loggedIn", username != null);
        returnStatus.put("username", username);

        return ResponseEntity.ok(returnStatus);

    }

    @PostMapping("/auth")
    public JwtResponse authenticate(@RequestBody JwtRequest jwtRequest) throws BadCredentialsException{
        try{
            // Authenticate the current jwt request
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            jwtRequest.getUsername(),
                            jwtRequest.getPassword()
                    )
            );
        }catch (BadCredentialsException e){
            throw new BadCredentialsException("Invalid Credential", e);
        }

        // Check the user details
        final UserDetails userDetails
                = userDetailsServiceImpl.loadUserByUsername(jwtRequest.getUsername());

        // Generate token when login succeeded.
        final String token
                = jwtUtility.generateToken(userDetails);

        return new JwtResponse(token);
    }
}
