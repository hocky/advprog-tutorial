package csui.advpro2021.auth.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.auth.model.JwtRequest;
import csui.advpro2021.auth.service.UserDetailsServiceImpl;
import csui.advpro2021.auth.utility.JWTUtility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class HomeControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private JWTUtility jwtUtility;

    @MockBean
    private UserDetailsServiceImpl userDetailsServiceImpl;

    private UserDetails userDetails;
    private String token;

    @MockBean
    private AuthenticationManager authenticationManager;

    @BeforeEach
    public void setUp(){
        userDetails = new User("1906285604", "123", new ArrayList<>());
        token = "ASDASD";
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testAuthenticateSuccessfully() throws Exception {

        when(jwtUtility.getUsernameFromAuthorizationString(anyString())).thenReturn("1906285604");

        mvc.perform(get("/check")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.loggedIn").value(true));
    }

    @Test
    public void testAuthenticateFailed() throws Exception {

        when(jwtUtility.getUsernameFromAuthorizationString(anyString())).thenReturn(null);

        mvc.perform(get("/check")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.loggedIn").value(false));
    }

    @Test
    public void testAuthenticateBadCredentials() throws Exception {
        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
                .thenThrow(new BadCredentialsException("Invalid Credential"));
        JwtRequest request = new JwtRequest();
        request.setUsername("1906285604");
        request.setPassword("bukan123");

        mvc.perform(post("/auth")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(request)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testAuthenticateGoodCredentials() throws Exception {
        JwtRequest request = new JwtRequest();
        request.setUsername("1906285604");
        request.setPassword("123");

        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
                .thenReturn(any(Authentication.class));

        when(userDetailsServiceImpl.loadUserByUsername(request.getUsername()))
                .thenReturn(userDetails);

        when(jwtUtility.generateToken(any(UserDetails.class))).thenReturn(token);

        mvc.perform(post("/auth")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(request)))
                .andExpect(status().isOk());

    }

}
