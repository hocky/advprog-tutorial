package csui.advpro2021.auth.utility;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.auth.model.JwtRequest;
import csui.advpro2021.auth.service.UserDetailsServiceImpl;
import csui.advpro2021.auth.utility.JWTUtility;
import csui.advpro2021.tais.model.Mahasiswa;
import io.jsonwebtoken.ExpiredJwtException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.lenient;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class JWTUtilityTest{

    @Autowired
    private JWTUtility jwtUtility;

    private UserDetails userDetails, badUser;

    @BeforeEach
    public void setUp(){
        userDetails = new User("1906285604", "123", new ArrayList<>());
        badUser = new User("190285604", "123", new ArrayList<>());
    }

    @Test
    public void validateToken() {
        String tmpToken = jwtUtility.generateToken(userDetails);
        String authorizationString = "Bearer " + tmpToken;
        assertEquals(jwtUtility.getUsernameFromAuthorizationString(authorizationString), "1906285604");
        assertTrue(jwtUtility.validateToken(tmpToken, userDetails));
        assertFalse(jwtUtility.validateToken(tmpToken, badUser));
        assertTrue(jwtUtility.isAuthorized(authorizationString, "1906285604"));
        Exception exception = assertThrows(ExpiredJwtException.class, () -> {
            jwtUtility.validateToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxOTA2Mjg1NjA0IiwiZXhwIjoxNjE3NjUwNzg4LCJpYXQiOjE2MTc2NTA3ODd9.aH1NLoXP3nJIAgv8wlCGJ6dOKdrRVvmcJhnroqHnEypNRGBE24bWxJ_ddOu5Swv59PkyvfsQV-GdFV0Fp3K2iw", userDetails);
        });
    }


    @Test
    public void validateBadToken() {
        String tmpToken = jwtUtility.generateToken(userDetails);
        String authorizationString = "Ber " + tmpToken;
        assertEquals(jwtUtility.getUsernameFromAuthorizationString(authorizationString), null);
    }

    @Test
    public void validateNullToken() {
        String tmpToken = jwtUtility.generateToken(userDetails);
        String authorizationString = null;
        assertEquals(jwtUtility.getUsernameFromAuthorizationString(authorizationString), null);
    }
}
