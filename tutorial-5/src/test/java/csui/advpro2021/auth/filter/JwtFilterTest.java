package csui.advpro2021.auth.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.auth.model.JwtRequest;
import csui.advpro2021.auth.service.UserDetailsServiceImpl;
import csui.advpro2021.auth.utility.JWTUtility;
import csui.advpro2021.tais.model.Mahasiswa;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class JwtFilterTest {

    @Mock
    JWTUtility jwtUtility;

    @Mock
    private FilterChain filterChain;

    @Mock
    UserDetailsServiceImpl userDetailsServiceImpl;

    @Mock
    private SecurityContextHolder securityContextHolder;

    @InjectMocks
    JwtFilter jwtFilter;

    private UserDetails userDetails;


    @BeforeEach
    public void setUp(){

        userDetails = new User("1906285604", "123", new ArrayList<>());
    }
    @Test
    public void validateToken() throws IOException, ServletException {
        String token = jwtUtility.generateToken(userDetails);
        String authorization = "Bearer " + token;

        when(jwtUtility.getTokenFromAuthorizationString(authorization))
                .thenReturn(token);
        when(jwtUtility.getUsernameFromToken(token))
                .thenReturn("1906285604");

        when(userDetailsServiceImpl.loadUserByUsername("1906285604"))
                .thenReturn(userDetails);
        when(jwtUtility.validateToken(token, userDetails))
                .thenReturn(true);

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", authorization);
        jwtFilter.doFilterInternal(request, new MockHttpServletResponse(), filterChain);
    }

}
