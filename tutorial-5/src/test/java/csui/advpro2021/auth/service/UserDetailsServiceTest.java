package csui.advpro2021.auth.service;


import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.MahasiswaService;
import io.jsonwebtoken.ExpiredJwtException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceTest {
    @Mock
    MahasiswaService mahasiswaService;
    @InjectMocks
    UserDetailsServiceImpl userDetailsService;
    @Test
    public void testLoadUserByUsernameNotFound(){
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(null);

        Exception exception = assertThrows(UsernameNotFoundException.class, () -> {
            userDetailsService.loadUserByUsername("1906285604");
        });
    }

    @Test
    public void testLoadUserByUsernameFound(){
        Mahasiswa mahasiswa = new Mahasiswa(
                "1906285604",
                "Hocky Yudhiono",
                "hocky.yudhiono@ui.ac.id",
                "4.0",
                "081352523339"
        );
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);
        UserDetails user = userDetailsService.loadUserByUsername("1906285604");
        assertEquals(user.getUsername(), "1906285604");
    }
}
