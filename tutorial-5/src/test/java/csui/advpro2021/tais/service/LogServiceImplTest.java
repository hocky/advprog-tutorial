package csui.advpro2021.tais.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MataKuliahService mataKuliahService;

    @Mock
    private MahasiswaService mahasiswaService;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;


    @InjectMocks
    private LogServiceImpl logService;

    private MataKuliah matkul1, matkul2;
    private Mahasiswa mahasiswa1, mahasiswa2;
    private Log log1, log2, log3, log4;

    @BeforeEach
    public void setUp(){
        matkul1 = new MataKuliah(
                "ANUM",
                "Analisis Numerik",
                "Ilmu Komputer"
        );

        matkul2 = new MataKuliah(
                "ADPRO",
                "Pemrograman Lanjut",
                "Sistem Informasi"
        );

        mahasiswa1 = new Mahasiswa(
                "1906285604",
                "Hocky Yudhiono",
                "hocky.yudhiono@ui.ac.id",
                "4.0",
                "081352523339"
        );

        mahasiswa2 = new Mahasiswa(
                "1906192052",
                "Maung Meong",
                "maung@cs.ui.ac.id",
                "3.4",
                "080810818103"
        );

        log1 = new Log(
                "Lab",
                "04-04-2021 13:01",
                "04-04-2021 15:02"
        );

        log2 = new Log(
                "LabAyam",
                "05-04-2021 08:54",
                "05-04-2021 20:12"
        );

        log3 = new Log(
                "LabSapi",
                "05-02-2021 08:54",
                "05-02-2021 20:12"
        );


        log4 = new Log(
                "LabKerbau",
                "05-02-2021 08:54",
                "05-01-2021 20:12"
        );
    }

    @Test
    public void testServiceAbleToRegisterMahasiswa() throws JsonProcessingException {

        matkul1.setAsdos(new ArrayList<>());

        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa1);
        when(mataKuliahService.getMataKuliah(anyString())).thenReturn(matkul1);

        when(mahasiswaRepository.save(any(Mahasiswa.class))).thenReturn(mahasiswa1);
        when(mataKuliahRepository.save(any(MataKuliah.class))).thenReturn(matkul1);

        logService.registerToMataKuliah(mahasiswa1.getNpm(), matkul1.getKodeMatkul());

        verify(mahasiswaService, times(1)).getMahasiswaByNPM(anyString());
        verify(mataKuliahService, times(1)).getMataKuliah(anyString());

        assertNotNull(mahasiswa1.getMataKuliah());
        assertEquals(mahasiswa1.getMataKuliah().getKodeMatkul(), matkul1.getKodeMatkul());

    }

    @Test
    public void testServiceAbleToCreateLog(){
        mahasiswa1.setLogMahasiswa(new ArrayList<>());

        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa1);

        when(mahasiswaRepository.save(any(Mahasiswa.class))).thenReturn(mahasiswa1);
        when(logRepository.save(any(Log.class))).thenReturn(log1);

        logService.createLog(mahasiswa1.getNpm(), log1);

        assertNotNull(mahasiswa1.getLogMahasiswa());
        assertEquals(mahasiswa1.getLogMahasiswa().size(), 1);
        assertEquals(log1.getMahasiswa(), mahasiswa1);

        verify(mahasiswaService, times(1)).getMahasiswaByNPM(anyString());
        verify(logRepository, times(1)).save(any(Log.class));
    }

    @Test
    public void testServiceCanDelete(){
        doNothing().when(logRepository).deleteById(anyInt());

        logService.deleteLogByKodeLog(1);
    }

    @Test
    public void testServiceCanGetLogOfMahasiswa(){
        List<Log> listLog = new ArrayList<>();
        listLog.add(log1);
        listLog.add(log2);
        listLog.add(log3);
        listLog.add(log4);
        mahasiswa1.setLogMahasiswa(
                listLog
        );
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa1);
        LogSummary result = logService.getLogOfMahasiswa(mahasiswa1.getNpm(), "April");
        assertEquals(result.getJamKerja(),
                (log1.getDurasiKerja() + log2.getDurasiKerja())/60.0);
        assertEquals(result.getPembayaran(), (long)(result.getJamKerja() * result.getPayRate()));
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa1);
        List<Log> logFiltered = (List<Log>) logService.getLogByWaktuMulai(mahasiswa1.getNpm(), "April");
        System.out.println(logFiltered);
        assertEquals(logFiltered.size(), 2);
    }

    @Test
    public void testServiceCanGetLogSummary(){

        List<Log> listLog = new ArrayList<>();
        listLog.add(log1);
        listLog.add(log2);
        listLog.add(log3);
        listLog.add(log4);

        mahasiswa1.setLogMahasiswa(listLog);

        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa1);
        List<LogSummary> result = (List) logService.getLogSummary(mahasiswa1.getNpm());
        assertEquals(result.size(), 2);
        LogSummary fourthMonth = result.get(1);
        assertEquals(fourthMonth.getPembayaran(), (long)(fourthMonth.getJamKerja() * fourthMonth.getPayRate()));
    }

    @Test
    public void testServiceCanGetByKodeLog(){
        when(logRepository.findByKodeLog(anyInt())).thenReturn(log1);

        Log result = logService.logRepository.findByKodeLog(log1.getKodeLog());
        assertEquals(result, log1);
    }

    @Test
    public void testServiceCanUpdateAccordingly(){
        when(logRepository.findByKodeLog(anyInt())).thenReturn(log1);
        when(logRepository.save(any(Log.class))).thenReturn(log2);

        Log result = logService.updateLog(log1.getKodeLog(), log2);
        assertEquals(log2.getDeskripsi(), log1.getDeskripsi());
        assertEquals(log2.getWaktuMulai().getMinute(), log1.getWaktuMulai().getMinute());
        assertEquals(log2.getWaktuAkhir().getMinute(), log1.getWaktuAkhir().getMinute());
    }
}
