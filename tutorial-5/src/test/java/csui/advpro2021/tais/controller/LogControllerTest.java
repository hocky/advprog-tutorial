package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import csui.advpro2021.auth.utility.JWTUtility;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogService logService;

    private MataKuliah matkul;
    private Mahasiswa mahasiswa;
    private Log log;
    private String bearerToken;

    @Autowired
    JWTUtility jwtUtility;

    @BeforeEach
    public void setUp(){
        matkul = new MataKuliah(
                "ANUM",
                "Analisis Numerik",
                "Ilmu Komputer"
        );

        mahasiswa = new Mahasiswa(
                "1906285604",
                "Hocky Yudhiono",
                "hocky.yudhiono@ui.ac.id",
                "4.0",
                "081352523339"
        );

        UserDetails userDetails = new User(mahasiswa.getNpm(), mahasiswa.getPassword(), new ArrayList<>());
        bearerToken = "Bearer " + jwtUtility.generateToken(userDetails);

        log = new Log(
                "Lab",
                "04-04-2021 13:01",
                "04-04-2021 15:02"
        );
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testControllerNpmRegisterMataKuliah() throws Exception{

        when(logService.registerToMataKuliah(anyString(), anyString())).thenReturn(mahasiswa);

        mvc.perform(get("/log/register/" + matkul.getKodeMatkul())
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapToJson(mahasiswa)));
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testControllerCreateLog() throws Exception {
        when(logService.createLog(anyString(), any(Log.class))).thenReturn(log);
        String jsonBody = mapToJson(log);

        mvc.perform(post("/log")
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Lab"));
    }
    
    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testDeleteLogByKodeLog() throws Exception {

        log.setMahasiswa(mahasiswa);
        when(logService.getLog(anyInt())).thenReturn(log);

        when(logService.deleteLogByKodeLog(anyInt())).thenReturn(200);

        mvc.perform(delete("/log/1")
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
        verify(logService, times(1)).getLog(anyInt());
        verify(logService, times(1)).deleteLogByKodeLog((anyInt()));
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testGetMonthSummary() throws Exception{

        List <Log> logFiltered = new ArrayList<>();
        logFiltered.add(new Log(
                "Lab",
                "04-04-2021 13:01",
                "04-04-2021 15:02"
        ));
        LogSummary logSummary = LogSummary.getMonthlyLog(logFiltered, "April");
        when(logService.getLogOfMahasiswa(anyString(), anyString())).thenReturn(logSummary);
        when(logService.getLogByWaktuMulai(anyString(), anyString())).thenReturn(logFiltered);

        Map<String, Object> returnResult = new TreeMap<>();
        returnResult.put("summary", logSummary);
        returnResult.put("daftarLog", logFiltered);

        mvc.perform(get("/log/summary/January")
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(returnResult)));
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testGetAllSummary() throws Exception{
        List<LogSummary> listSummary = new ArrayList<>();
        listSummary.add(new LogSummary("January", 10));
        listSummary.add(new LogSummary("March", 12));
        when(logService.getLogSummary(anyString())).thenReturn(listSummary);
        mvc.perform(get("/log/summary")
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(listSummary)));
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testControllerGetLogByKodeLog() throws Exception{

        log.setMahasiswa(mahasiswa);
        when(logService.getLog(anyInt())).thenReturn(log);

        mvc.perform(get("/log/" + log.getKodeLog())
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Lab"));
    }

    @Test
    public void testControllerGetNonExistLog() throws Exception{

        log.setMahasiswa(mahasiswa);
        when(logService.getLog(anyInt())).thenReturn(log);

        mvc.perform(get("/log/12").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testControllerUpdateLog() throws Exception{

        Log log1 = new Log(
                "Lab3",
                "04-04-2021 13:01",
                "04-04-2021 15:02"
        );

        log.setMahasiswa(mahasiswa);
        when(logService.getLog(anyInt())).thenReturn(log);

        when(logService.updateLog(anyInt(), any(Log.class))).thenReturn(log);
        String jsonBody = mapToJson(log);
        mvc.perform(put("/log/" + log1.getKodeLog())
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Lab"));
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testControllerUpdateLogNegative() throws Exception{

        String jsonBody = "{\n" +
                "    \"deskripsi\": \"Lab\",\n" +
                "    \"waktuMulai\": \"04-05-2021 13:01\",\n" +
                "    \"waktuAkhir\": \"04-04-2021 15:02\"\n" +
                "}";


        log.setMahasiswa(mahasiswa);
        when(logService.getLog(anyInt())).thenReturn(log);

        when(logService.updateLog(anyInt(), any(Log.class))).thenReturn(log);
        mvc.perform(put("/log/1")
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testControllerNonAuthorized() throws Exception{

        Log log2 = new Log(
                "asw",
                "04-04-2021 13:01",
                "04-04-2021 15:02"
        );

        Mahasiswa mahasiswa1 = new Mahasiswa(
                "1906604",
                "Hocky Yudhiono",
                "hocky.yudhiono@ui.ac.id",
                "4.0",
                "081352523339"
        );

        log2.setMahasiswa(mahasiswa1);

        log.setMahasiswa(mahasiswa);
        when(logService.getLog(anyInt())).thenReturn(log2);

        mvc.perform(get("/log/12").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testDeleteLogByKodeLogNonAuthorized() throws Exception {

        Log log2 = new Log(
                "asw",
                "04-04-2021 13:01",
                "04-04-2021 15:02"
        );

        Mahasiswa mahasiswa1 = new Mahasiswa(
                "1906604",
                "Hocky Yudhiono",
                "hocky.yudhiono@ui.ac.id",
                "4.0",
                "081352523339"
        );

        log2.setMahasiswa(mahasiswa1);

        log.setMahasiswa(mahasiswa);
        when(logService.getLog(anyInt())).thenReturn(log2);

        mvc.perform(delete("/log/1")
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
        verify(logService, times(1)).getLog(anyInt());
    }

    @Test
    @WithMockUser(username = "1906285604", authorities = { "ADMIN", "USER" })
    public void testUpdateLogByKodeLogNonAuthorized() throws Exception {

        Log log2 = new Log(
                "asw",
                "04-04-2021 13:01",
                "04-04-2021 15:02"
        );

        Mahasiswa mahasiswa1 = new Mahasiswa(
                "1906604",
                "Hocky Yudhiono",
                "hocky.yudhiono@ui.ac.id",
                "4.0",
                "081352523339"
        );

        log2.setMahasiswa(mahasiswa1);

        when(logService.getLog(anyInt())).thenReturn(log2);

        String jsonBody = mapToJson(log);

        mvc.perform(put("/log/1")
                .header(HttpHeaders.AUTHORIZATION, bearerToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(status().isForbidden());

        verify(logService, times(1)).getLog(anyInt());
    }

}
