package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.auth.service.UserDetailsServiceImpl;
import csui.advpro2021.auth.utility.JWTUtility;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MahasiswaControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    private String bearerToken;
    private Mahasiswa mahasiswa;

    @Autowired
    private JWTUtility jwtUtility;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        UserDetails userDetails = new User(mahasiswa.getNpm(), mahasiswa.getPassword(), new ArrayList<>());
        bearerToken = "Bearer " + jwtUtility.generateToken(userDetails);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerPostMahasiswa() throws Exception{

        when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);

        mvc.perform(post("/mahasiswa")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(mahasiswa)))
                .andExpect(jsonPath("$.npm").value("1906192052"));
    }

    @Test
    public void testControllerGetListMahasiswa() throws Exception{

        Iterable<Mahasiswa> listMahasiswa = Arrays.asList(mahasiswa);
        when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);

        mvc.perform(get("/mahasiswa").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].npm").value("1906192052"));
    }

    @Test
    public void testControllerGetMahasiswaByNpm() throws Exception{

        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        mvc.perform(get("/mahasiswa/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value("Maung Meong"));
    }

    @Test
    public void testControllerGetNonExistMahasiswa() throws Exception{
        mvc.perform(get("/mahasiswa/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "1906192052", authorities = { "ADMIN", "USER" })
    public void testControllerUpdateMahasiswa() throws Exception{

        mahasiswaService.createMahasiswa(mahasiswa);

        //Update mahasiswa object nama
        mahasiswa.setNama("Quanta Quantul");

        when(mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa)).thenReturn(mahasiswa);

        mvc.perform(put("/mahasiswa").contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(mahasiswa))
                .header(HttpHeaders.AUTHORIZATION, bearerToken))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value("Quanta Quantul"));
    }

    @Test
    @WithMockUser(username = "1906192052", authorities = { "ADMIN", "USER" })
    public void testControllerDeleteMahasiswa() throws Exception{
        mahasiswaService.createMahasiswa(mahasiswa);
        mvc.perform(delete("/mahasiswa")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, bearerToken))
                .andExpect(status().isNoContent());
    }



}
